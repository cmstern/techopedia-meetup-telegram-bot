﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using System.Diagnostics;
using Telegram.Bot.Types.InputMessageContents;
using System.Text;

namespace TelegramBotDemo
{
    public class Program
    {
        private static readonly TelegramBotClient Bot = new TelegramBotClient(ConfigurationManager.AppSettings["AccessToken"]);

        static DateTime meetupTime = DateTime.Parse("4/30/17 7:30 PM");
        static float Latitude = 40.595242f;
        static float Longitude = -73.960966f;
        static string Address = "2524 Coney Island Ave Brooklyn, NY — 11223";
        static string AddressMapUrl = "https://www.google.com/maps/place/2524+Coney+Island+Ave,+Brooklyn,+NY+11223/@40.5952342,-73.9634402,17z/data=!3m1!4b1!4m5!3m4!1s0x89c244f514183201:0x608e1390ac230fba!8m2!3d40.5952301!4d-73.9612515";
        static int MeetupNumber = 5;
        static string pictureoflocation = @"/Pictures/kasaihibachi.jpg";
        static string menuUrl = @"http://www.kasaihibachi.com/menu/";
        static string pastMeetups = @"Meetup #1: Abigael's on Broadway
Meetup #2: Nobo Wine and Grill
Meetup #3: Essen NY Deli
Meetup #4: Bison And Bourbon
Meetup #5: Kasaihibachi";
        public static List<string> users = new List<string>();

        static void Main(string[] args)
        {
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            var me = Bot.GetMeAsync().Result;

            Console.Title = me.Username;

            LoadDataToFile();
            Bot.StartReceiving();
            Console.ReadLine();
            Bot.StopReceiving();
        }
        #region Events

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            LogToFile("EX: " + receiveErrorEventArgs.ApiRequestException.Message);
            Debugger.Break();
        }

        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            try
            {
                switch (chosenInlineResultEventArgs.ChosenInlineResult.ResultId)
                {
                    case "Get Location":
                    //SendLocation(mes)
                    default:
                        break;
                }
                Console.WriteLine($"Received chosen inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
            }
            catch (Exception ex)
            {
                LogToFile("EX: " + ex.Message);
            }
        }

        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            InlineQueryResult[] results = {
                new InlineQueryResultLocation
                {
                    Id = "1",
                    Latitude = Latitude, // displayed result
                    Longitude = Longitude,
                    Title = Address ,
                    InputMessageContent = new InputLocationMessageContent // message if result is selected
                    {
                        Latitude = Latitude,
                        Longitude = Longitude,
                    }
                },
            };

            await Bot.AnswerInlineQueryAsync(inlineQueryEventArgs.InlineQuery.Id, results, isPersonal: true, cacheTime: 0);
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            try
            {
                var newLine = Environment.NewLine;
                var message = messageEventArgs.Message;
                var from = messageEventArgs.Message.From;
                var user = from.FirstName + " " + from.LastName + " (" + from.Username + ")";

                LogToFile($"Message from: {user}{newLine}Text: {message.Text}");
                if (message == null || message.Type != MessageType.TextMessage) return;
                var text = message.Text;

                if (message.Text.StartsWith("/start")) // send inline keyboard
                {
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                    var keyboard = new InlineKeyboardMarkup(new[]
                    {
                    new[] // first row
                    {
                        new InlineKeyboardButton("Get Location 🛰")
                    },
                    new[] // second row
                    {
                        new InlineKeyboardButton("Info"),
                        new InlineKeyboardButton("🍔🍺🌮 Menu"),
                    },
                    new[] // third row
                    {
                        new InlineKeyboardButton("Photos 📸"),
                        new InlineKeyboardButton("Bot Menu"),
                    },
                    new[] // fourth row
                    {
                        new InlineKeyboardButton("Trivia ❓❓"),
                    }
                });

                    await Bot.SendTextMessageAsync(message.Chat.Id, "Tech👌🏻pedia Meetup 🕗",
                        replyMarkup: keyboard);
                }
                else if (text.StandardizeString() == "menu") // send custom keyboard
                {
                    if (message.Chat.Type != ChatType.Private)
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Sorry but my menu I only show in private 😉",
                            replyMarkup: new ReplyKeyboardHide()); return;
                    }
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                    new [] // first row
                    {
                        new KeyboardButton("🍗"),
                        new KeyboardButton("🍖"),
                        new KeyboardButton("🍤"),
                        new KeyboardButton("🌭"),
                    },
                    new [] // last row
                    {
                        new KeyboardButton("🍺"),
                        new KeyboardButton("🍽"),
                        new KeyboardButton("💩"),
                    }
                });

                    await Bot.SendTextMessageAsync(message.Chat.Id, "Choose",
                        replyMarkup: keyboard);
                }
                else if (text.StandardizeString() == "pictureoflocation")
                {
                    if (string.IsNullOrWhiteSpace(pictureoflocation))
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Sorry no picture available for this time");return;
                    }
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                    string file = AppDomain.CurrentDomain.BaseDirectory + pictureoflocation;

                    var fileName = file.Split('\\').Last();

                    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var fts = new FileToSend(fileName, fileStream);
                        await Bot.SendPhotoAsync(message.Chat.Id, fts, "Here is the place");
                    }
                }
                else if (message.Text.StartsWith("help")) // request location or contact
                {
                    var keyboard = new ReplyKeyboardMarkup(new[]
                    {
                    new KeyboardButton("Send my location")
                    {
                        RequestLocation = true
                    },
                    new KeyboardButton("Send my contact")
                    {
                        RequestContact = true
                    },
                });

                    await Bot.SendTextMessageAsync(message.Chat.Id, "Send you're info so someone can try to help you", replyMarkup: keyboard);
                }
                else if (message.Text.StartsWith("myinfo")) // for testing get info
                {
                    var info = $@"message.Date: {message.Date}
{newLine}message.From.Username: {message.From.Username}
{newLine}message.From.FirstName: {message.From.FirstName}
{newLine}message.From.LastName: {message.From.LastName}
{newLine}message.From.Id: {message.From.Id}";

                    await Bot.SendTextMessageAsync(message.Chat.Id, info);
                }
                else if (text.StandardizeString() == "imcoming")
                {
                    if (AddUser(user))
                        await Bot.SendTextMessageAsync(message.Chat.Id, "ok get ya! " + GetFooterMessage(message));
                    else
                        await Bot.SendTextMessageAsync(message.Chat.Id, "I understand the first time!" + GetFooterMessage(message));
                }
                else if (text.StandardizeString() == "icantcome")
                {
                    var reply = RemoveUser(user) ? "sorry to see you go! " : "you (" + user + ") are not on the list!";
                    await Bot.SendTextMessageAsync(message.Chat.Id, reply + GetFooterMessage(message));
                }
                else if (text.StandardizeString() == "location")
                {
                    await SendLocation(message);
                }
                else if (text.StandardizeString() == "showpastmeetups")
                {
                    var response = new StringBuilder();
                    response.AppendLine("<code>Techopedia</code> <i>Meetups</i> 👩‍👩‍👧");
                    response.AppendLine(pastMeetups);
                    response.AppendLine("<a href=\"https://goo.gl/photos/FafMoG8sXAMmUZzc6\">Here's Some Photos</a> (you can add some too!)");

                    await Bot.SendTextMessageAsync(message.Chat.Id, response + GetFooterMessage(message),parseMode:ParseMode.Html);
                }
                else if (new string[]{"CMStern","AgDev"}.Contains(from.Username) && text.StandardizeString() == "getlist")
                {
                    var users = System.IO.File.ReadAllText(GetDataFilePath()).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                    var response = "Ok we got " + users.Count() + " people coming\n\n";
                    foreach (var usr in users)
                    {
                        response += "\n " + usr + " 🌟";
                    }
                    await Bot.SendTextMessageAsync(message.Chat.Id, response + GetFooterMessage(message));
                }
                else
                {
                    if(message.Chat.Type == ChatType.Private)
                    {
                        var reply = $"Techopedia Meetup {MeetupNumber}!";

                        reply += newLine + $"/Im_Coming{newLine}/I_Cant_Come{newLine}/Location{newLine}/Picture_of_Location";
                        await Bot.SendTextMessageAsync(message.Chat.Id, reply + GetFooterMessage(message),
                            replyMarkup: new ReplyKeyboardHide());
                    }
                }

            }
            catch (Exception ex)
            {
                LogToFile("EX: " + ex.Message);
            }
        }


        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            try
            {
                var message = callbackQueryEventArgs.CallbackQuery.Message;
                switch (callbackQueryEventArgs.CallbackQuery.Data)
                {
                    case "Get Location 🛰":
                        await SendLocation(message);
                        break;
                    case "Info":
                        await Bot.SendTextMessageAsync(message.Chat.Id, $"<i>Past</i> <code>Techopedia</code> <i>Meetups</i> 👩‍👩‍👧 \n{pastMeetups}\n" + GetTimeToMeettup() + "\n\n<a href=\"https://goo.gl/photos/FafMoG8sXAMmUZzc6\">See Photos</a> (you can add some too!)", parseMode: ParseMode.Html);
                        break;
                    case "🍔🍺🌮 Menu":
                        await Bot.SendTextMessageAsync(message.Chat.Id, menuUrl,
                            replyMarkup: new ReplyKeyboardHide());
                        //await SendFile(message, @"/Pictures/menu.pdf");
                        break;
                    case "Bot Menu":
                        await Bot.SendTextMessageAsync(message.Chat.Id, "/start \n/Im_Coming \n/I_Cant_Come \n/Location \n/Picture_of_Location \n/Show_Past_Meetups ",
                            replyMarkup: new ReplyKeyboardHide());
                        break;
                    case "Photos 📸":
                        await Bot.SendTextMessageAsync(message.Chat.Id, "Click <a href=\"https://goo.gl/photos/FafMoG8sXAMmUZzc6\">Here</a> to see photos of the past meetups \nP.s. You can add some too!", parseMode: ParseMode.Html);
                        break;
                    case "Trivia ❓❓":
                        int result = (new Random()).Next(12);
                        var list = new string[] { "Who made a siyum 📚 by one of the meetups? And where was it?", "By which meetup did someone have a bday party 🎉🎊? Was it his real bday?", "Who was locked out of his phone 📱? And why? Who did it?", "How long did we wait on the couches 🛋🛋 of Abigael's on Broadway? Was it awkward meeting for the first time? 😉", "Who arranged the first meetup 👨‍👨‍👦‍👦👨‍👨‍👦‍👦?", "How many oz was the biggest steak 🍖🍗 at Abigael's? did you finish it?", "Who took a picture 📸 of us in Nobo's? was the person taking the pic standing on a chair? and who took the pictures 📸 outside?", "Who did you ride 🚗 with to the first meetup? and to the second? if you were the driver did you enjoy your company 👨‍👨‍👦‍👦 ?", "Were you freezing your ears off 🙄 standing outside Nobo's after the meetup?", "Honestly you didn't imagine the ppl you met at the meetup to look the way they do, or did you 😬 ?", "Which funny 😀 moment do you remember from one of the party's?", "Have you met 5 ppl from Techopedia™ in person outside of a meetup?" };
                        var randomMessage = list[result];
                        await Bot.SendTextMessageAsync(message.Chat.Id, $"{randomMessage}",
                            replyMarkup: new ReplyKeyboardHide());
                        break;
                    default:
                        await Bot.AnswerCallbackQueryAsync(callbackQueryEventArgs.CallbackQuery.Id,
                            $"No action set for action: {callbackQueryEventArgs.CallbackQuery.Data}");
                        LogToFile($"No: {callbackQueryEventArgs.CallbackQuery.Data}", true);
                        break;
                }
            }
            catch (Exception ex)
            {
                LogToFile("EX: " + ex.Message);
            }
        }

        #endregion
        #region Sheared Reply
        public static async Task SendFile(Message message, string localFilePath, string caption = "")
        {
            await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadDocument);
            string file = AppDomain.CurrentDomain.BaseDirectory + localFilePath;
            var fileName = file.Split('\\').Last();
            using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var fts = new FileToSend(fileName, fileStream);
                await Bot.SendDocumentAsync(message.Chat.Id, fts, caption);
            }
        }
        public static async Task SendLocation(Message message)
        {
            await Bot.SendLocationAsync(message.Chat.Id, Latitude, Longitude, false, message.MessageId);
            await Bot.SendTextMessageAsync(message.Chat.Id, $"{Address}{Environment.NewLine}{AddressMapUrl}", true, true);
        }
        #endregion
        static string GetTimeToMeettup()
        {
            TimeSpan meetupDuration = (meetupTime - DateTime.Now).Duration();
            return meetupTime > DateTime.Now ?
                "\nUpcoming Meetup will be in " + DescribeTime(meetupDuration.Days," Day") + DescribeTime(meetupDuration.Hours, "Hour") + DescribeTime(meetupDuration.Minutes, "Minute") + DescribeTime(meetupDuration.Seconds, "Second")  :
                " Time is up! Meetup on the go! 🍷 \nPsst.. You can add your photos to the album!";
        }
        static string DescribeTime(int amount,string part)
        {
            return amount > 0 ? $"{amount} {part}{(amount > 1 ? "s " : " ")}" : " ";
        }

        static string GetFooterMessage(Message message)
        {
            var footer = String.Empty;
            if (message.Date < DateTime.Now.AddSeconds(-30))
            {
                footer = "\n\nSorry for the late response, i was sleeping 😴. yes bots sleep too❕❕";
            }
            footer += Environment.NewLine + GetTimeToMeettup();
            return footer;
        }
        public static void LogToFile(string message, bool isError = false)
        {
            Console.WriteLine(message);
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (isError) { path += "\\TechopediaBot.log"; } else { path += "\\TechopediaBotError.log"; }
            System.IO.File.AppendAllText(path, DateTime.Now + "  " + message + Environment.NewLine);
            Console.WriteLine(DateTime.Now + "  " + message + Environment.NewLine);
        }
        public static string GetDataFilePath()
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\data.txt";
            if (!System.IO.File.Exists(path)) System.IO.File.Create(path);
            while (IsFileLocked(path))
            {
                Thread.Sleep(10);
            }
            return path;
        }
        public static void LoadDataToFile()
        {
            users = System.IO.File.ReadAllLines(GetDataFilePath()).ToList();
        }
        public static void SaveDataToFile()
        {
            System.IO.File.WriteAllLines(GetDataFilePath(),users.ToArray());
        }
        public static bool AddUser(string user)
        {
            var alreadyInList = users.Contains(user);
            if (!alreadyInList) users.Add(user);
            SaveDataToFile();
            return !alreadyInList;
        }
        public static bool RemoveUser(string user)
        {
            var existInList = users.Contains(user);
            users.RemoveAll(u => u == user);
            SaveDataToFile();
            return existInList;
        }
        public static bool IsFileLocked(string path)
        {
            try
            {
                using (FileStream fs = System.IO.File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    fs.Close();
                }
                // The file is not locked
                return false;
            }
            catch (Exception)
            {
                // The file is locked
                return true;
            }
        }
    }

    public static class ExtensionMethods
    {
        public static string StandardizeString(this string str)
        {
            return str.ToLower().Replace(" ", "").Replace("'", "").Replace("/", "").Replace("_", "").Replace("@techopediabot", "");
        }
    }
}
